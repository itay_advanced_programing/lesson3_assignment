#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;

#define DATABASE_NAME "carsDealer.db"
#define AVAILABLE 1

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

/*
Function will check if we can buy a car
Input: the id of the buyer and the car, the database and error message
Output: true or false
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	int balance = 0;
	int price = 0;
	int isAvailable = 0;
	string message;
	string value;

	rc = sqlite3_open(DATABASE_NAME, &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	//Getting the balance of the account
	message = "select balance from accounts where id=" + to_string(buyerid);
	cout << message << endl;
	
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	value = results.begin()->second[0];
	balance = atoi(value.c_str());

	clearTable();
	//Getting the price of the car
	message = "select price from cars where id=" + to_string(carid);

	cout << message << endl;
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	value = results.begin()->second[0];
	price = atoi(value.c_str());

	clearTable();
	//Getting the available
	message = "select available from cars where id=" + to_string(carid);

	cout << message << endl;
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	value = results.begin()->second[0];
	isAvailable = atoi(value.c_str());
	clearTable();

	if ((balance >= price) && (isAvailable == AVAILABLE)) //Checking if we can buy
	{
		//Buying
		balance = balance - price;
		message = "update accounts set balance=" + to_string(balance);
		message = message + " where id=";
		message = message + to_string(buyerid);
		
		cout << message << endl;

		rc = sqlite3_exec(db, message.c_str(), callback, NULL, &zErrMsg); //Changing the balance
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		clearTable();
		message = "update cars set available=0 where id=" + to_string(carid);

		cout << message << endl;

		rc = sqlite3_exec(db, message.c_str(), callback, NULL, &zErrMsg); //Changing the available row to 0
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		sqlite3_close(db);

		return true;
	}

	sqlite3_close(db);
	return false;
}

/*
Function will transfer money from account A to account B
Input: the source and dest accounts, the amount of money, the database and the error message
Output: true or false
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string message;
	string value;
	int balanceOfSource;
	int balanceOfDest;

	rc = sqlite3_open(DATABASE_NAME, &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	clearTable();

	cout << "begin transaction" << endl;
	rc = sqlite3_exec(db, "begin transaction", callback, NULL, &zErrMsg); //Begin transaction
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	message = "select balance from accounts where id=" + to_string(from); //Getting the balance from the source account
	cout << message << endl;
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	value = results.begin()->second[0];
	balanceOfSource = atoi(value.c_str());
	clearTable();

	message = "select balance from accounts where id=" + to_string(to); //Getting the balance from the dest account
	cout << message << endl;
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	value = results.begin()->second[0];
	balanceOfDest = atoi(value.c_str());
	clearTable();

	if (balanceOfSource < amount) //Checking if we can transfer the money
	{
		cout << "Can't make the trasfer. There is not enough money in the source account" << endl;
		rc = sqlite3_exec(db, "end transaction", callback, NULL, &zErrMsg); //End transaction
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		return false;
	}

	//Updating the balance in the source account
	message = "update accounts set balance=" + to_string(balanceOfSource - amount) + " where id=" + to_string(from);
	cout << message << endl;
	rc = sqlite3_exec(db, message.c_str(), callback, NULL, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	//Updating the balance in the dest account
	message = "update accounts set balance=" + to_string(balanceOfDest + amount) + " where id=" + to_string(to);
	cout << message << endl;
	rc = sqlite3_exec(db, message.c_str(), callback, NULL, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	cout << "end transaction" << endl;
	rc = sqlite3_exec(db, "end transaction", callback, NULL, &zErrMsg); //End transaction
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	sqlite3_close(db);

	return true;
}

/*
Function will check who can buy a car by its ID
Input: the car id, the database and the error message
Output: none
*/
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc;
	int price;
	string message;

	rc = sqlite3_open(DATABASE_NAME, &db);

	cout << "select price from cars where id=" + to_string(carId) << endl;
	clearTable();
	message = "select price from cars where id=" + to_string(carId);
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg); //Getting the price of the car
	price = atoi(results.begin()->second[0].c_str());
	clearTable();
	cout << "select * from accounts where balance > " + to_string(price) << endl;
	message = "select * from accounts where balance > " + to_string(price);
	rc = sqlite3_exec(db, message.c_str(), callback, 0, &zErrMsg); //Checking who has enough money to but the car

	printTable(); //Printing
	clearTable();

	sqlite3_close(db);
}

int main()
{
	sqlite3* db;
	char *zErrMsg = 0;
	int i = 0;
	int buyId = 0;
	int carId = 0;
	int sourceId = 0;
	int destId = 0;
	int amount = 0;
	
	int rc;

	rc = sqlite3_open(DATABASE_NAME, &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	/*
	Printing the cars and accounts tables at the begining for the user
	*/

	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	clearTable();

	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	clearTable();
	
	system("pause");
	
	for (i = 0; i < 3; i++) //PART A
	{
		cout << "Enter buyer id" << endl;
		cin >> buyId;
		cout << "Enter car id" << endl;
		cin >> carId;

		if (carPurchase(buyId, carId, db, zErrMsg))
		{
			cout << "success!" << endl;
			rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			else
			{
				printTable();
			}
			clearTable();
			rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			else
			{
				printTable();
			}
			clearTable();

		}
		else
		{
			cout << "fail..." << endl;
		}
	}
	
	system("pause");

	//Part B

	cout << "Enter source Id" << endl;
	cin >> sourceId;
	cout << "Enter dest Id" << endl;
	cin >> destId;
	cout << "Enter amount of money" << endl;
	cin >> amount;


	if (balanceTransfer(sourceId, destId, amount, db, zErrMsg))
	{
		cout << "success" << endl;

		rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		else
		{
			printTable();
		}
	}
	else
	{
		cout << "fail" << endl;
	}

	system("pause");

	//Part C

	cout << "Enter car id" << endl;
	cin >> carId;

	whoCanBuy(carId, db, zErrMsg);

	sqlite3_close(db);

	system("pause");
	return 0;
}